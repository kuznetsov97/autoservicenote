package com.mymobileapp.serviceautobook;

import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.Models.WorkUnit;
import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class UnitTest{

    public DatabaseAdapter adapter = new DatabaseAdapter();

    //1,7
    @Test
    public void Way1() {
        List<WorkUnit> list = new ArrayList<WorkUnit>();

        list.add(new WorkUnit(0, 0, 0, "",
                new Date(3,12,2018), 0, "", 0));

        List<WorkUnit> trueList = list; //сортироваться будет один элемент, поэтому проверка будет проводиться с ним же

        assertEquals(adapter.quickSort(list, 0, list.size()-1), trueList);
    }

    //1,2,3,5,6,7
    @Test
    public void Way3() {
        List<WorkUnit> list = new ArrayList<WorkUnit>();

        list.add(new WorkUnit(0, 0, 0, "",
                new Date(2018,12,3), 0, "", 0));
        list.add(new WorkUnit(1, 0, 0, "",
                new Date(2018,12,4), 0, "", 0));

        List<WorkUnit> wrongList = new ArrayList<WorkUnit>();

        wrongList.add(list.get(1));
        wrongList.add(list.get(0));

        assertEquals(adapter.quickSort(wrongList, 0, wrongList.size()-1), list);

    }

    //1,2,3,4,5,6,7
    @Test
    public void Way4() {
        List<WorkUnit> list = new ArrayList<WorkUnit>();

        list.add(new WorkUnit(0, 0, 0, "",
                new Date(2018,12,4), 0, "", 0));
        list.add(new WorkUnit(1, 0, 0, "",
                new Date(2018,12,3), 0, "", 0));

        List<WorkUnit> trueList = list;

        assertEquals(adapter.quickSort(list, 0, list.size()-1), trueList);
    }
}
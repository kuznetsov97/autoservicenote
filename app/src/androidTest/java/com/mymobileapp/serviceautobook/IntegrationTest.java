package com.mymobileapp.serviceautobook;

import android.support.test.InstrumentationRegistry;

import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseHelper;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.WorkUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IntegrationTest{

    //getEnteredAuto
    @Test
    public void IntegrationStep1() {
        DatabaseAdapter adapter = new DatabaseAdapter();
        adapter.setDatabaseHelperContext(new DatabaseHelper(InstrumentationRegistry.getTargetContext()));
        adapter.open();

        DatabaseAdapter mockAdapter = mock(DatabaseAdapter.class);

        Auto auto = adapter.getEnteredAuto();//то, что должно выдаваться заглушкой

        //заглушка
        when(mockAdapter.getEnteredAuto()).thenReturn(auto);

        List<WorkUnit> tester = adapter.getAllWorks(mockAdapter.getEnteredAuto());//результат метода через заглушку
        List<WorkUnit> tested = adapter.getAllWorks(auto);//результат реальной связки методов

        //Сравнение результатов производится по сравнению параметров в каждом элементе списка
        for(int i=0; i< tester.size(); i++) {

            WorkUnit testerItem = tester.get(i);
            WorkUnit testedItem = tested.get(i);

            assertEquals(testerItem.getId(), testedItem.getId());
            assertEquals(testerItem.getAutoId(), testedItem.getAutoId());
            assertEquals(testerItem.getWorkTypeId(), testedItem.getWorkTypeId());
            assertEquals(testerItem.getDate(), testedItem.getDate());
            assertEquals(testerItem.getDescription(), testedItem.getDescription());
            assertEquals(testerItem.getCost(), testedItem.getCost());
            assertEquals(testerItem.getKm(), testedItem.getKm());
            assertEquals(testerItem.getWorker(), testedItem.getWorker());
        }
    }

    @Test
    public void IntegrationStep2() {
        DatabaseAdapter adapter = new DatabaseAdapter();
        adapter.setDatabaseHelperContext(new DatabaseHelper(InstrumentationRegistry.getTargetContext()));
        adapter.open();

        DatabaseAdapter mockAdapter = mock(DatabaseAdapter.class);

        Auto auto = adapter.getEnteredAuto();//то, что должно поступать на вход методов
        List<WorkUnit> unsortList = adapter.getAllWorks(auto);//то, что должно выдаваться заглушкой


        //заглушка
        when(mockAdapter.getAllWorks(auto)).thenReturn(unsortList);

        List<WorkUnit> tester = adapter.getAllWorksSort(mockAdapter.getAllWorks(auto));//результат метода через заглушку
        List<WorkUnit> tested = adapter.getAllWorksSort(unsortList);//результат реальной связки методов

        //Сравнение результатов производится по сравнению параметров в каждом элементе списка
        for(int i=0; i< tester.size(); i++) {

            WorkUnit testerItem = tester.get(i);
            WorkUnit testedItem = tested.get(i);

            assertEquals(testerItem.getId(), testedItem.getId());
            assertEquals(testerItem.getAutoId(), testedItem.getAutoId());
            assertEquals(testerItem.getWorkTypeId(), testedItem.getWorkTypeId());
            assertEquals(testerItem.getDate(), testedItem.getDate());
            assertEquals(testerItem.getDescription(), testedItem.getDescription());
            assertEquals(testerItem.getCost(), testedItem.getCost());
            assertEquals(testerItem.getKm(), testedItem.getKm());
            assertEquals(testerItem.getWorker(), testedItem.getWorker());
        }
    }
}

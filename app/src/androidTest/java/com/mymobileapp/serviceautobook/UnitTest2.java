package com.mymobileapp.serviceautobook;

import android.support.test.InstrumentationRegistry;

import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseHelper;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.WorkUnit;
import org.junit.Test;
import java.util.List;
import static junit.framework.TestCase.assertEquals;

//getAllWorks
public class UnitTest2{

    //Имеется две записи в базе данных
    @Test
    public void UnitWay1() {
        DatabaseAdapter adapter = new DatabaseAdapter();
        adapter.setDatabaseHelperContext(new DatabaseHelper(InstrumentationRegistry.getTargetContext()));
        adapter.open();

        List<WorkUnit> works = adapter.getAllWorks(new Auto(1,"ВАЗ","2101",1978,"т256хс",1) );

        assertEquals(works.size(), 2);
    }

    //Записи в базе отсутствуют
    @Test
    public void UnitWay2() {
        DatabaseAdapter adapter = new DatabaseAdapter();
        adapter.setDatabaseHelperContext(new DatabaseHelper(InstrumentationRegistry.getTargetContext()));
        adapter.open();

        List<WorkUnit> works = adapter.getAllWorks(new Auto(4,"ВАЗ","2110",2000,"т256хс",1) );

        assertEquals(works.size(), 0);
    }
}

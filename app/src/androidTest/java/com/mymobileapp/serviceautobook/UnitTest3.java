package com.mymobileapp.serviceautobook;

import android.support.test.InstrumentationRegistry;

import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseHelper;
import com.mymobileapp.serviceautobook.Models.Auto;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

//getEnteredAuto
public class UnitTest3 {

    //Авто выбрано
    @Test
    public void UnitWay1() {
        DatabaseAdapter adapter = new DatabaseAdapter();
        adapter.setDatabaseHelperContext(new DatabaseHelper(InstrumentationRegistry.getTargetContext()));
        adapter.open();

        adapter.setEnteredAuto(2);
        Auto auto = adapter.getEnteredAuto();

        assertEquals(auto.getId(), 2);
    }

    //Выбранного авто нет
    @Test
    public void UnitWay2() {
        DatabaseAdapter adapter = new DatabaseAdapter();
        adapter.setDatabaseHelperContext(new DatabaseHelper(InstrumentationRegistry.getTargetContext()));
        adapter.open();


        Auto auto = adapter.getEnteredAuto();
        auto.setEntered(0);
        adapter.updateAuto(auto);

        Auto TestedAuto = adapter.getEnteredAuto();

        assertEquals(TestedAuto, null);

        adapter.setEnteredAuto(1);//для корректности дальнейшей работы программы
    }
}

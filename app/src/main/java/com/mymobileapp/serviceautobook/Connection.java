package com.mymobileapp.serviceautobook;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mymobileapp.serviceautobook.DTO.marksAndModelsDTO;
import com.mymobileapp.serviceautobook.DTO.serviceDTO;
import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.ServiceWork;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Connection extends AsyncTask<String, Void, String> {

    private Auto auto;
    private DatabaseAdapter adapter;

    public void Initialize(DatabaseAdapter adapter) {
        this.adapter = adapter;
        adapter.open();
        auto = adapter.getEnteredAuto();
        adapter.close();
    }
    @Override
    protected String doInBackground(String... path) {
        String content="";

        if(path[0] == "getService") {
            try {
                content = QueryLoadServices();
                if (content != "Запрос не вернул результатов" && content != "Ошибка") {
                    Type listType = new TypeToken<ArrayList<serviceDTO>>() {}.getType();
                    List<serviceDTO> myList = new Gson().fromJson(content, listType);
                    adapter.open();
                    for (serviceDTO item : myList) {
                        adapter.insertSW(new ServiceWork(0, (int) auto.getId(), item.name, new Date(), item.perioddate, item.periodkm, item.type, 0, 1));
                    }
                    adapter.close();
                }
            } catch (Exception ex) {
                content = ex.getMessage();
            }
            content = "getService";
            return content;
        }
        if(path[0] == "getMarksModels") {
            try{
                content = QueryAllMarksModels();
                if (content != "Запрос не вернул результатов" && content != "Ошибка") {
                    Type listType = new TypeToken<ArrayList<marksAndModelsDTO>>() {}.getType();
                    List<marksAndModelsDTO> myList = new Gson().fromJson(content, listType);
                    adapter.markModel = myList;
                }
            }
            catch(Exception e){
                content = e.getMessage();
            }
        }
        content = "getMarks";

        return content;
    }

    public String QueryLoadServices() throws IOException, JSONException {
        URL url = new URL("http://192.168.1.34:82/api_for_mobile_client/getworks.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoOutput(true);
        urlConnection.setDoInput(true);

        try {
            JSONObject postDataParams = new JSONObject();
            postDataParams.put("mark", auto.getMark());
            postDataParams.put("model", auto.getModel());

            OutputStream outputStream = urlConnection.getOutputStream();
            outputStream.write(postDataParams.toString().getBytes("UTF-8"));

            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }

    public String QueryAllMarksModels() throws IOException {
        URL url = new URL("http://192.168.1.34:82/api_for_mobile_client/getmarksmodels.php");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        urlConnection.setDoInput(true);

        try {
            urlConnection.connect();
            int responseCode= urlConnection.getResponseCode();
            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String answer = bufferedReader.readLine();

                return answer;
            }
            else{
                return "Ошибка";
            }
        }
        finally {
            urlConnection.disconnect();
        }
    }

    AutoCompleteTextView selBox;
    Context context;
    public void setViewElem(AutoCompleteTextView selBox, Context context)
    {
        this.context = context;
        this.selBox = selBox;
    }
    @Override
    protected void onPostExecute(String content) {
        if(content == "getMarks") {
            String[] markArr = null;
            if (adapter.markModel != null) {

                markArr = new String[adapter.markModel.size()];
                int i = 0;
                for (marksAndModelsDTO item : adapter.markModel) {
                    markArr[i] = item.name;
                    i++;
                }
                selBox.setAdapter(new ArrayAdapter<>(context,
                        android.R.layout.simple_dropdown_item_1line, markArr));
            }
        }
        if(content == "getService") {
            adapter.open();
            int sys = 0;
            for(ServiceWork item : adapter.getAllServiceWorks(auto)){
                if(item.getIsSystem() == 1) sys++;
            }
            adapter.close();
            if(sys == 0) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

                builder.setMessage("Не удалось найти записей по данной модели авто")
                        .setTitle(R.string.dialog_title_warning);
                android.app.AlertDialog dialog = builder.create();
                dialog.show();
            }
            else{
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

                builder.setMessage("Данные загружены, перейдите во вкладку напоминаний")
                        .setTitle(R.string.dialog_title_warning);
                android.app.AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }
}


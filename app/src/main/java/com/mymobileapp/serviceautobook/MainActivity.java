package com.mymobileapp.serviceautobook;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.mymobileapp.serviceautobook.Adapters.MainScreensPagerAdapter;
import com.mymobileapp.serviceautobook.Fragments.ServiceFragment;
import com.mymobileapp.serviceautobook.Models.ServiceWork;

public class MainActivity extends AppCompatActivity implements
        ViewPager.OnPageChangeListener, MyDialog.IOnChangheDialog {

    private Context context;

    public MainScreensPagerAdapter pagerAdapter;
    private ViewPager viewPager;

    private boolean isChangedFromBottomNavigation = false;
    private boolean isChangedFromViewPager = false;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if (!isChangedFromViewPager) {
                isChangedFromBottomNavigation = true;
            }
            switch (item.getItemId()) {
                case R.id.navigation_general_info:
//                    Toast.makeText(context, "Общая информация", Toast.LENGTH_SHORT).show();
                    viewPager.setCurrentItem(0);
                    pagerAdapter.update(1);
                    return true;
                case R.id.navigation_repairs:
//                    Toast.makeText(context, "Ремонты", Toast.LENGTH_SHORT).show();
                    viewPager.setCurrentItem(1);
                    pagerAdapter.update(2);
                    return true;
                case R.id.navigation_general_expenses:
//                    Toast.makeText(context, "Общие расходы", Toast.LENGTH_SHORT).show();
                    viewPager.setCurrentItem(2);
                    pagerAdapter.update(3);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        context = this;

        Bundle extras = getIntent().getExtras();
        int numPage = 0;
        if (extras != null) {
            numPage = extras.getInt("page");
        }

        pagerAdapter = new MainScreensPagerAdapter(getSupportFragmentManager(), getApplicationContext());
        viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(numPage);
        viewPager.addOnPageChangeListener(this);
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    //Implementing OnPageChangeListener
    @Override
    public void onPageScrolled(int i, float v, int i1) {
    }

    @Override
    public void onPageSelected(int i) {
        if (isChangedFromBottomNavigation) {
            return;
        }
        BottomNavigationView navigation = findViewById(R.id.navigation);
        int navItem = 0;
        switch (i) {
            case 0:
                navItem = R.id.navigation_general_info;
                break;
            case 1:
                navItem = R.id.navigation_repairs;
                break;
            case 2:
                navItem = R.id.navigation_general_expenses;
                break;
        }
        navigation.setSelectedItemId(navItem);
    }

    @Override
    public void onPageScrollStateChanged(int i) {
        isChangedFromBottomNavigation = false;
    }
    //End of implementation

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        pagerAdapter.update(3);
    }
}

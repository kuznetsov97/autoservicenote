package com.mymobileapp.serviceautobook;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.WorkType;
import com.mymobileapp.serviceautobook.Models.WorkUnit;

import java.util.Date;
import java.util.List;

public class MyDialog extends DialogFragment {

    private DatabaseAdapter adapter;

    public interface IOnChangheDialog
    {
        public void onDialogPositiveClick(DialogFragment dialog);
    }

    IOnChangheDialog mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (IOnChangheDialog) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public void InitializeDbAdapter(DatabaseAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_create_work, null);

        final String descr = getArguments().getString("descr");
        final String type = getArguments().getString("type");

        builder.setView(view)
                .setPositiveButton(R.string.title_ok_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        TextView cost = (TextView)view.findViewById(R.id.cost);
                        TextView place = (TextView)view.findViewById(R.id.working_place);
                        TextView km = (TextView)view.findViewById(R.id.km);
                        adapter.open();
                        adapter.insertUOW(new WorkUnit(0,
                                (int)adapter.getEnteredAuto().getId(),
                                (int)adapter.getWorkTypeByName(type).getId(),
                                descr,
                                new Date(),
                                Integer.parseInt(cost.getText().toString()),
                                place.getText().toString(),
                                Integer.parseInt(km.getText().toString()))
                        );
                        Auto auto = adapter.getEnteredAuto();
                        auto.setSumCost(auto.getSumCost() + Integer.parseInt(cost.getText().toString()));
                        adapter.updateAuto(auto);
                        adapter.close();
                        mListener.onDialogPositiveClick(MyDialog.this);
                    }
                })
                .setNegativeButton(R.string.title_cancel_btn, null);
        return builder.create();
    }
}

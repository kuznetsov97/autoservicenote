package com.mymobileapp.serviceautobook.Models;

public class WorkType {
    private long Id;
    private  String Name;

    public WorkType(long id, String name){
        Id = id;
        Name = name;
    }

    public long getId() {return Id;}
    public String getName(){return Name;}

    public void setName(String value){
        Name = value;
    }
}

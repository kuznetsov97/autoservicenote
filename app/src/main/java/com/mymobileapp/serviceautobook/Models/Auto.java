package com.mymobileapp.serviceautobook.Models;

import android.content.Intent;

import java.util.Date;

public class Auto {
    private long Id;
    private String Mark;
    private String Model;
    private int Year;
    private String Number;
    private int Entered;
    private String PhotoPath;
    private int SumCost;

    public Auto(long id, String mark, String model, int year, String number, String photoPath, int sumCost, int entered){
        Id = id;
        Mark = mark;
        Model = model;
        Year = year;
        Number = number;
        PhotoPath = photoPath;
        SumCost = sumCost;
        Entered = entered;
    }

    public long getId(){return Id;}
    public String getMark(){return Mark;}
    public String getModel(){return Model;}
    public int getYear(){return Year;}
    public String getNumber(){return Number;}
    public int getEntered(){return Entered;}
    public String getPhotoPath(){return PhotoPath;}
    public int getSumCost(){
        if(SumCost >= 0) return SumCost;
        else return 0;
    }

    public void setEntered(int value){Entered = value;}
    public void setMark(String value){Mark = value;}
    public void setModel(String value){Model = value;}
    public void setYear(int value) {
        String d = (new Date()).toString();
        String split[] = d.split(" ");
        String y = split[split.length-1];
        int year = Integer.parseInt(y);

        if(value>1850 && value < year) {
            Year = value;
        }
    }
    public void setNumber(String value) {
        if(value.length()>=8 && value.length()<=9) {
            String numStr = value.substring(1,3);
            int num = 0;
            try{
                num = Integer.parseInt(numStr);
            } catch(Exception e){}

            if(num!=0) {
                String[] masStr = {value.substring(0,0), value.substring(4,4), value.substring(5,5)};
                String Letters = "АВЕКМНОРСТУХ";
                boolean suc = true;
                for(int i = 0; i<3; i++) {
                    if(!Letters.contains(masStr[i])){
                        suc = false;
                        break;
                    }
                }
                if(suc) {
                    int reg = 0;
                    try{
                        reg = Integer.parseInt(value.substring(6));
                    } catch(Exception e){}
                    if(reg != 0){
                        Number = value;
                    }
                }
            }
        }
    }
    public void setSumCost(int value) {
        if (value >= 0) SumCost = value;
    }

    public void setPhotoPath(String value) {PhotoPath = value;}
}

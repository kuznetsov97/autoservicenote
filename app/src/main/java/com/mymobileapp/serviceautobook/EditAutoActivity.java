package com.mymobileapp.serviceautobook;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Time;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;

import com.mymobileapp.serviceautobook.DTO.marksAndModelsDTO;
import com.mymobileapp.serviceautobook.DTO.modelDTO;
import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.WorkType;
import com.mymobileapp.serviceautobook.Models.WorkUnit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EditAutoActivity extends AppCompatActivity {

    AutoCompleteTextView markBox;
    AutoCompleteTextView modelBox;
    EditText numberBox;
    EditText yearBox;
    ImageView photoBox;
    String nameFile;

    Button delButton;

    private DatabaseAdapter adapter;
    private long autoId =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_auto_activity);
        //кнопка "Возврат"
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        adapter = new DatabaseAdapter(this);

        markBox = findViewById(R.id.mark);
        modelBox = findViewById(R.id.model);
        numberBox = (EditText) findViewById(R.id.number);
        yearBox = (EditText) findViewById(R.id.year);
        photoBox = (ImageView) findViewById(R.id.photo);

        Button button = (Button)findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, 1);
            }
        });



        Connection connect = new Connection();
        connect.Initialize(adapter);
        connect.setViewElem(markBox, this);
        connect.execute("getMarksModels");

        markBox.setOnDismissListener(new AutoCompleteTextView.OnDismissListener() {
            @Override
            public void onDismiss() {
                List<modelDTO> Models = null;
                for (marksAndModelsDTO item : adapter.markModel) {
                 if (markBox.getText().toString().equals(item.name)) {
                     Models = item.models;
                     break;
                 }
                }
                String[] modelArr = null;
                if (Models != null) {
                 modelArr = new String[Models.size()];
                 int i = 0;
                 for (modelDTO item : Models) {
                     modelArr[i] = item.name;
                     i++;
                 }
                 modelBox.setAdapter(new ArrayAdapter<>(returnContext(),
                         android.R.layout.simple_dropdown_item_1line, modelArr));
                }
            }
        });

        delButton = (Button) findViewById(R.id.deleteButton);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            autoId = extras.getLong("id");
        }
        // если 0, то добавление
        if (autoId > 0) {
            // получаем элемент по id из бд
            adapter.open();

            Auto auto = adapter.getEnteredAuto();
            markBox.setText(auto.getMark());
            modelBox.setText(auto.getModel());
            numberBox.setText(auto.getNumber());
            yearBox.setText(String.valueOf(auto.getYear()));
            photoBox.setImageURI(Uri.parse(auto.getPhotoPath()));
            adapter.close();
        } else {
            // скрываем кнопку удаления
            delButton.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        Bitmap bitmap = null;

        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    photoBox.setImageBitmap(bitmap);
                }
        }
    }

    private String SavePicture(ImageView iv, String folderToSave) {
        OutputStream fOut = null;
        Time time = new Time();
        time.setToNow();

        try {
            nameFile = Integer.toString(time.year) + Integer.toString(time.month) + Integer.toString(time.monthDay) + Integer.toString(time.hour) + Integer.toString(time.minute) + Integer.toString(time.second) +".jpg";
            File file = new File(folderToSave, nameFile); // создать уникальное имя для файла основываясь на дате сохранения
            fOut = new FileOutputStream(file);

            Bitmap bitmap = ((BitmapDrawable) iv.getDrawable()).getBitmap();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut); // сохранять картинку в jpeg-формате с 85% сжатия.
            fOut.flush();
            fOut.close();
            //MediaStore.Images.Media.insertImage(getContentResolver(), file.getAbsolutePath(), file.getName(),  file.getName()); // регистрация в фотоальбоме
        }
        catch (Exception e) // здесь необходим блок отслеживания реальных ошибок и исключений, общий Exception приведен в качестве примера
        {
            return e.getMessage();
        }
        return "";
    }

    public void save(View view) throws ParseException {
        int cost = 0;
        if(autoId > 0) {
            adapter.open();
            Auto oldAuto = adapter.getAutoById(autoId);
            adapter.close();
            if (oldAuto != null) cost = oldAuto.getSumCost();
        }
        SavePicture(photoBox, this.getCacheDir().toString());
        Auto auto = new Auto(
                autoId,
                markBox.getText().toString(),
                modelBox.getText().toString(),
                Integer.parseInt(yearBox.getText().toString()),
                numberBox.getText().toString(),
                this.getCacheDir().toString()+"/"+nameFile,
                cost,
                1
        );

        adapter.open();
        if (autoId > 0) {
            adapter.updateAuto(auto);
        } else {
            adapter.insertAuto(auto);
        }
        adapter.close();
        goHome();
    }
    public void delete(View view){
        adapter.open();
        adapter.deleteAuto(autoId);
        adapter.close();
        goHome();
    }
    public Context returnContext(){return this;}

    private void goHome(){
        // переход к главной activity
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

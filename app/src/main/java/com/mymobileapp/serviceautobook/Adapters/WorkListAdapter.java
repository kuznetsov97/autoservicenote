package com.mymobileapp.serviceautobook.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.Models.WorkType;
import com.mymobileapp.serviceautobook.Models.WorkUnit;
import com.mymobileapp.serviceautobook.R;

import java.util.List;

public class WorkListAdapter extends ArrayAdapter<WorkUnit> {

    private LayoutInflater inflater;
    private int layout;
    private List<WorkUnit> works;

    private DatabaseAdapter adapter;

    public WorkListAdapter(Context context, int resource, List<WorkUnit> works, DatabaseAdapter adapter) {
        super(context, resource, works);
        this.adapter = adapter;
        this.works = works;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=inflater.inflate(this.layout, parent, false);

        TextView dayView = (TextView) view.findViewById(R.id.day);
        TextView monthView = (TextView) view.findViewById(R.id.month);

        TextView typeView = (TextView) view.findViewById(R.id.type);
        ImageView imageTypeView = (ImageView) view.findViewById(R.id.image_type);

        TextView descrView = (TextView) view.findViewById(R.id.descr);

        TextView costView = (TextView) view.findViewById(R.id.cost);
        TextView kmView = (TextView) view.findViewById(R.id.km);

        WorkUnit work = works.get(position);

        adapter.open();
        WorkType type = adapter.getWorkTypeById(work.getWorkTypeId());
        adapter.close();

        String month;
        switch (work.getDate().getMonth())
        {
            case 0: month = "янв"; break;
            case 1: month = "фев"; break;
            case 2: month = "мар"; break;
            case 3: month = "апр"; break;
            case 4: month = "май"; break;
            case 5: month = "июн"; break;
            case 6: month = "июл"; break;
            case 7: month = "авг"; break;
            case 8: month = "сен"; break;
            case 9: month = "окт"; break;
            case 10: month = "ноя"; break;
            case 11: month = "дек"; break;
            default: month = "..."; break;
        }

        dayView.setText(String.valueOf(work.getDate().getDate()));
        monthView.setText(month);

        typeView.setText(type.getName());
        int idImage = 0;
        switch (typeView.getText().toString())
        {
            case "Двигатель": idImage = R.drawable.engine; break;
            case "Подвеска": idImage = R.drawable.suspension; break;
            case "Трансмиссия": idImage = R.drawable.transmission; break;
            case "Интерьер": idImage = R.drawable.interier; break;
            case "Экстерьер": idImage = R.drawable.exterier; break;
            case "Прочее": idImage = R.drawable.others; break;
        }
        imageTypeView.setImageResource(idImage);
        descrView.setText(work.getDescription());
        costView.setText(work.getCost()+" руб");
        kmView.setText(work.getKm()+" км");

        return view;
    }
}
package com.mymobileapp.serviceautobook.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mymobileapp.serviceautobook.Fragments.AutoesFragment;
import com.mymobileapp.serviceautobook.Fragments.ServiceFragment;
import com.mymobileapp.serviceautobook.Fragments.WorksFragment;
import com.mymobileapp.serviceautobook.R;

public class MainScreensPagerAdapter extends FragmentPagerAdapter {

    private Fragment[] fragments;
    private String[] titles;

    public MainScreensPagerAdapter(FragmentManager fman, Context con) {
        super(fman);

        fragments = new Fragment[3];

        // Каждой страничке ViewPager соответствует свой фрагмент.
        // Тут я создаю объекты и забиваю их в массив
        fragments[0] = new AutoesFragment();
        fragments[1] = new WorksFragment();
        fragments[2] = new ServiceFragment();

        titles = new String[3];
        titles[0] = con.getString(R.string.title_general_info);
        titles[1] = con.getString(R.string.title_repairs);
        titles[2] = con.getString(R.string.title_service_book);

    }

    @Override
    public Fragment getItem(int i) {
        return  fragments[i];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    int updateData=0;

    //call this method to update fragments in ViewPager dynamically
    public void update(int xyzData) {
        this.updateData = xyzData;
        notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof WorksFragment) {
            ((WorksFragment) object).update(updateData);
        }
        if (object instanceof ServiceFragment) {
            ((ServiceFragment) object).update(updateData);
        }
        if (object instanceof AutoesFragment) {
            ((AutoesFragment) object).update(updateData);
        }
        //don't return POSITION_NONE, avoid fragment recreation.
        return super.getItemPosition(object);
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        return titles[position];
    }
}

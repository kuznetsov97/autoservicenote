package com.mymobileapp.serviceautobook.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.R;

import java.util.List;

public class AutoListAdapter extends ArrayAdapter<Auto> {

    private LayoutInflater inflater;
    private int layout;
    private List<Auto> autoes;

    public AutoListAdapter(Context context, int resource, List<Auto> autoes) {
        super(context, resource, autoes);
        this.autoes = autoes;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=inflater.inflate(this.layout, parent, false);

        TextView markView = (TextView) view.findViewById(R.id.mark);
        TextView modelView = (TextView) view.findViewById(R.id.model);
        TextView numberView = (TextView) view.findViewById(R.id.number);


        Auto auto = autoes.get(position);

        markView.setText(auto.getMark());
        modelView.setText(auto.getModel());
        numberView.setText(auto.getNumber());

        return view;
    }
}
package com.mymobileapp.serviceautobook.DTO;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class marksAndModelsDTO {
    @SerializedName("name")
    public String name;
    @SerializedName("models")
    public List<modelDTO> models = new ArrayList<modelDTO>();
}

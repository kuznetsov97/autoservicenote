package com.mymobileapp.serviceautobook;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mymobileapp.serviceautobook.Adapters.AutoListAdapter;
import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.Fragments.AutoesFragment;
import com.mymobileapp.serviceautobook.Models.Auto;

import java.util.List;

public class AutoListActivity extends AppCompatActivity {

    ListView autoList;
    AutoListAdapter listAutoAdapter;
    DatabaseAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_autoes);

        ActionBar actionBar =getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        adapter = new DatabaseAdapter(this);

        autoList = (ListView)findViewById(R.id.list_autoes);
        autoList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Auto auto = listAutoAdapter.getItem(position);
                adapter.open();
                adapter.setEnteredAuto(auto.getId());
                adapter.close();
                startActivity(new Intent(AutoListActivity.this, MainActivity.class));
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab_add_auto);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(v.getContext(), EditAutoActivity.class), 0);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        adapter.open();
        List<Auto> autoes = adapter.getAllAutoes();
        adapter.close();

        listAutoAdapter = new AutoListAdapter(this, R.layout.item_list_auto, autoes);
        autoList.setAdapter(listAutoAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}


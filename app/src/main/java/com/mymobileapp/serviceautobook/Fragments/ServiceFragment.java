package com.mymobileapp.serviceautobook.Fragments;

import android.app.AlertDialog;
import android.app.AppComponentFactory;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mymobileapp.serviceautobook.Adapters.ServiceListAdapter;
import com.mymobileapp.serviceautobook.Adapters.WorkListAdapter;
import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.EditServiceActivity;
import com.mymobileapp.serviceautobook.EditWorkActivity;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.ServiceWork;
import com.mymobileapp.serviceautobook.Models.WorkUnit;
import com.mymobileapp.serviceautobook.MyDialog;
import com.mymobileapp.serviceautobook.R;

import java.util.Date;
import java.util.List;

public class ServiceFragment extends Fragment implements IUpdatebleFragment{
    private ListView serviceList;
    private ServiceListAdapter listServiceAdapter;

    private Context context;
    private Auto auto;
    DatabaseAdapter adapter;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        context = getContext();

        serviceList = view.findViewById(R.id.list_service);

        adapter = new DatabaseAdapter(context);
        adapter.open();
        auto = adapter.getEnteredAuto();
        adapter.close();

        FloatingActionButton fab = view.findViewById(R.id.fab_service);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(context, EditServiceActivity.class), 0);
            }
        });

        serviceList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ServiceWork work = listServiceAdapter.getItem(position);
                if(work.getIsSystem() == 0) {
                    Intent intent = new Intent(context, EditServiceActivity.class);
                    intent.putExtra("id", work.getId());
                    startActivity(intent);
                }
                else
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                    builder.setMessage(R.string.message_unable_edit)
                            .setTitle(R.string.dialog_title_warning);

                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                return true;
            }
        });

        serviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ServiceWork service = listServiceAdapter.getItem(position);

                MyDialog dialog = new MyDialog();
                dialog.InitializeDbAdapter(adapter);
                Bundle args = new Bundle();
                args.putString("descr", service.getDescription());
                args.putString("type", service.getType());
                dialog.setArguments(args);
                dialog.show(getFragmentManager(), "custom");

                if(service.getIsOnlyEvent() == 1) {
                    adapter.open();
                    adapter.deleteSW(service.getId());
                    adapter.close();
                }
                service.setDateComplete(new Date());
                adapter.open();
                adapter.updateSW(service);
                adapter.close();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        adapter.open();
        List<ServiceWork> services = adapter.getAllServiceWorks(auto);
        adapter.close();

        listServiceAdapter = new ServiceListAdapter(context, R.layout.item_list_service, services, adapter);
        serviceList.setAdapter(listServiceAdapter);
    }

    @Override
    public void update(int data) {
        if(data == 3)
            onResume();
    }
}

package com.mymobileapp.serviceautobook.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.mymobileapp.serviceautobook.Adapters.AutoListAdapter;
import com.mymobileapp.serviceautobook.Adapters.WorkListAdapter;
import com.mymobileapp.serviceautobook.AutoListActivity;
import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.EditAutoActivity;
import com.mymobileapp.serviceautobook.EditWorkActivity;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.WorkUnit;
import com.mymobileapp.serviceautobook.R;

import java.util.List;

public class WorksFragment extends Fragment implements IUpdatebleFragment{

    private ListView workList;
    private WorkListAdapter listWorkAdapter;

    private Context context;
    private Auto auto;
    DatabaseAdapter adapter;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_works, container, false);
        context = getContext();

        workList = view.findViewById(R.id.list_works);

        adapter = new DatabaseAdapter(context);
        adapter.open();
        auto = adapter.getEnteredAuto();
        adapter.close();

        FloatingActionButton fab = view.findViewById(R.id.fab_repairs);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(context, EditWorkActivity.class), 0);
            }
        });

        workList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                WorkUnit work = listWorkAdapter.getItem(position);
                Intent intent = new Intent(context, EditWorkActivity.class);
                intent.putExtra("id", work.getId());
                startActivity(intent);
                return true;
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        adapter.open();
        List<WorkUnit> works = adapter.getAllWorks(auto);
        adapter.close();

        listWorkAdapter = new WorkListAdapter(context, R.layout.item_list_work, works, adapter);
        workList.setAdapter(listWorkAdapter);
    }

    @Override
    public void update(int data) {
        if(data == 2)
            onResume();
    }
}

package com.mymobileapp.serviceautobook.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mymobileapp.serviceautobook.AutoListActivity;
import com.mymobileapp.serviceautobook.Connection;
import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.EditAutoActivity;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.ServiceWork;
import com.mymobileapp.serviceautobook.R;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AutoesFragment extends Fragment implements IUpdatebleFragment{

    private TextView carName;
    private TextView sumCost;
    private ImageView photoAuto;

    private Context context;
    private Auto auto;
    DatabaseAdapter adapter;

    public AutoesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_autoes, container, false);
        context = getContext();

        adapter = new DatabaseAdapter(context);
        adapter.open();
        auto = adapter.getEnteredAuto();
        adapter.close();

        carName = view.findViewById(R.id.carName);
        sumCost = view.findViewById(R.id.sumCost);
        photoAuto = view.findViewById(R.id.photoAuto);

        carName.setText(auto.getMark() + " " + auto.getModel());
        sumCost.setText(String.valueOf(auto.getSumCost()));
        photoAuto.setImageResource(R.drawable.exterier);

        FloatingActionButton fab = view.findViewById(R.id.edit_entered_auto_info);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, EditAutoActivity.class);
                intent.putExtra("id", auto.getId());
                startActivityForResult(intent, 0);
            }
        });

        onAutoInfoChanged();

        Button btnOtherAutoes = view.findViewById(R.id.open_list_autoes);
        btnOtherAutoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //открытие списка автомобилей
                startActivity(new Intent(context, AutoListActivity.class));
            }
        });

        /*Button btnLoadServices = view.findViewById(R.id.load_list_works);
        btnLoadServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Проверка, загружался ли список ранее
                //boolean flag = false;
                //adapter.open();
                //List<ServiceWork> list = adapter.getAllServiceWorks(auto);
                //adapter.close();
                //for(ServiceWork s : list) {
                //    if(s.getIsSystem() == 1){
                //        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
//
                //        builder.setMessage(R.string.message_loaded_items)
                //                .setTitle(R.string.dialog_title_warning);
                //        android.app.AlertDialog dialog = builder.create();
                //        dialog.show();
                //        flag = true;
                //        break;
                //    }
                //}
                adapter.open();
                List<ServiceWork> serv = adapter.getAllServiceWorks(auto);
                for(ServiceWork w : serv){
                    if(w.getIsSystem() == 1)
                    adapter.deleteSW(w.getId());
                }
                adapter.close();

                //if(!flag) {
                    Connection connect = new Connection();
                    connect.Initialize(adapter);
                    connect.setViewElem(null, getActivity());
                    connect.execute("getService");



                //}
            }
        });*/
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        onAutoInfoChanged();
    }

    private void onAutoInfoChanged() {
        adapter.open();
        auto = adapter.getEnteredAuto();
        adapter.close();

        carName.setText(auto.getMark() + " " + auto.getModel());
        sumCost.setText(String.valueOf(auto.getSumCost()));
        photoAuto.setImageURI(Uri.parse(auto.getPhotoPath()));
    }

    @Override
    public void update(int data) {
        if(data == 1)
            onAutoInfoChanged();
    }

}

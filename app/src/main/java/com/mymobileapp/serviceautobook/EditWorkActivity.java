package com.mymobileapp.serviceautobook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.WorkType;
import com.mymobileapp.serviceautobook.Models.WorkUnit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EditWorkActivity extends AppCompatActivity {

    private SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    Spinner typeBox;
    EditText descrBox;
    DatePicker dateBox;
    EditText costBox;
    EditText workerBox;
    EditText kmBox;
    Button delButton;

    private DatabaseAdapter adapter;
    private long workId =0;
    private List<WorkType> workTypeColl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_work_activity);
        //кнопка "Возврат"
        ActionBar actionBar =getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        adapter = new DatabaseAdapter(this);
        adapter.open();
        workTypeColl = adapter.getAllTypesOfWork();
        adapter.close();
        String[] types = new String[workTypeColl.size()];
        for(int i =0; i<workTypeColl.size(); i++) {
            types[i] = workTypeColl.get(i).getName();
        }
        typeBox = (Spinner) findViewById(R.id.type);
        // Создаем адаптер ArrayAdapter с помощью массива строк и стандартной разметки элемета spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, types);
        // Определяем разметку для использования при выборе элемента
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Применяем адаптер к элементу spinner
        typeBox.setAdapter(spinnerAdapter);

        descrBox = (EditText) findViewById(R.id.descr);
        dateBox = (DatePicker) findViewById(R.id.date);
        costBox = (EditText) findViewById(R.id.cost);
        workerBox = (EditText) findViewById(R.id.worker);
        kmBox = (EditText) findViewById(R.id.km);

        delButton = (Button) findViewById(R.id.deleteButton);



        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            workId = extras.getLong("id");
        }
        // если 0, то добавление
        if (workId > 0) {
            // получаем элемент по id из бд
            adapter.open();
            try {
                WorkUnit work = adapter.getWorkById(workId);

                int selectedIndex=0;
                for(int i =0; i<workTypeColl.size(); i++) {
                    if(workTypeColl.get(i).getName().equals(adapter.getWorkTypeById(work.getWorkTypeId()).getName())){
                        selectedIndex = i;
                        break;
                    }
                }
                typeBox.setSelection(selectedIndex);
                descrBox.setText(work.getDescription());
                dateBox.updateDate(work.getDate().getYear()+1900, work.getDate().getMonth(), work.getDate().getDate());
                costBox.setText(String.valueOf(work.getCost()));
                workerBox.setText(work.getWorker());
                kmBox.setText(String.valueOf(work.getKm()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            adapter.close();
        } else {
            // скрываем кнопку удаления
            delButton.setVisibility(View.GONE);
        }
    }

    public void save(View view) throws ParseException {
        adapter.open();
        Auto auto = adapter.getEnteredAuto();
        int oldCost = 0;
        if(workId>0){
            WorkUnit oldWork = adapter.getWorkById(workId);
            oldCost = oldWork.getCost();
        }
        adapter.close();


        WorkUnit work = new WorkUnit(
                (int) workId,
                (int) auto.getId(),
                (int) workTypeColl.get(typeBox.getSelectedItemPosition()).getId(),
                descrBox.getText().toString(),
                new Date(dateBox.getYear()-1900, dateBox.getMonth(), dateBox.getDayOfMonth()),
                Integer.parseInt(costBox.getText().toString()),
                workerBox.getText().toString(), Integer.parseInt(kmBox.getText().toString())
        );

        adapter.open();
        if (workId > 0) {
            adapter.updateUOW(work);
            auto.setSumCost(auto.getSumCost() + Integer.parseInt(costBox.getText().toString()) - oldCost);
        } else {
            adapter.insertUOW(work);
            auto.setSumCost(auto.getSumCost() + Integer.parseInt(costBox.getText().toString()));
        }
        adapter.updateAuto(auto);
        adapter.close();
        goHome();
    }
    public void delete(View view) throws ParseException {
        adapter.open();
        Auto auto = adapter.getEnteredAuto();
        WorkUnit oldWork = adapter.getWorkById(workId);

        adapter.deleteUOW(workId);
        auto.setSumCost(auto.getSumCost() - oldWork.getCost());
        adapter.updateAuto(auto);

        adapter.close();
        goHome();
    }


    private void goHome(){

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("page", 1);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

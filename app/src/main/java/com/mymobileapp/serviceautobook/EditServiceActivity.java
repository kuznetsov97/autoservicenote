package com.mymobileapp.serviceautobook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.mymobileapp.serviceautobook.DataBaseHelpers.DatabaseAdapter;
import com.mymobileapp.serviceautobook.Fragments.ServiceFragment;
import com.mymobileapp.serviceautobook.Models.Auto;
import com.mymobileapp.serviceautobook.Models.ServiceWork;
import com.mymobileapp.serviceautobook.Models.WorkType;
import com.mymobileapp.serviceautobook.Models.WorkUnit;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EditServiceActivity extends AppCompatActivity {

    private SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    EditText descrBox;
    DatePicker dateBox;
    EditText periodBox;
    CheckBox onlyEventBox;
    Button delButton;

    private List<WorkType> workTypeColl;
    Spinner typeBox;

    private DatabaseAdapter adapter;
    private long serviceId =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_service_activity);
        //кнопка "Возврат"
        ActionBar actionBar =getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        adapter = new DatabaseAdapter(this);

        adapter.open();
        workTypeColl = adapter.getAllTypesOfWork();
        adapter.close();
        String[] types = new String[workTypeColl.size()];
        for(int i =0; i<workTypeColl.size(); i++) {
            types[i] = workTypeColl.get(i).getName();
        }
        typeBox = (Spinner) findViewById(R.id.type);
        // Создаем адаптер ArrayAdapter с помощью массива строк и стандартной разметки элемета spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, types);
        // Определяем разметку для использования при выборе элемента
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Применяем адаптер к элементу spinner
        typeBox.setAdapter(spinnerAdapter);

        descrBox = (EditText) findViewById(R.id.descr);
        dateBox = (DatePicker) findViewById(R.id.date);
        periodBox = (EditText) findViewById(R.id.period);
        onlyEventBox = (CheckBox) findViewById(R.id.check);


        onlyEventBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    LinearLayout layoutPeriod = (LinearLayout) findViewById(R.id.period_layout);
                    periodBox.setText("0");
                    layoutPeriod.setVisibility(View.INVISIBLE);

                }
                else {
                    LinearLayout layoutDate = (LinearLayout) findViewById(R.id.period_layout);
                    periodBox.setText("");
                    layoutDate.setVisibility(View.VISIBLE);
                }
            }
        });

        delButton = (Button) findViewById(R.id.deleteButton);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            serviceId = extras.getLong("id");
        }
        // если 0, то добавление
        if (serviceId > 0) {
            // получаем элемент по id из бд
            adapter.open();
            try {
                ServiceWork service = adapter.getServiceWorkById(serviceId);

                int selectedIndex=0;
                for(int i =0; i<workTypeColl.size(); i++) {
                    if(workTypeColl.get(i).getName().equals(adapter.getWorkTypeByName(service.getType()).getName())){
                        selectedIndex = i;
                        break;
                    }
                }
                typeBox.setSelection(selectedIndex);

                descrBox.setText(service.getDescription());
                dateBox.updateDate(service.getDateComplete().getYear(), service.getDateComplete().getMonth(), service.getDateComplete().getDate());
                periodBox.setText(String.valueOf(service.getPeriodDate()));
                if(service.getIsOnlyEvent() == 1) {
                    onlyEventBox.setChecked(true);
                }
                else {
                    onlyEventBox.setChecked(false);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
            adapter.close();
        } else {
            // скрываем кнопку удаления
            delButton.setVisibility(View.GONE);
        }
    }

    public void save(View view) throws ParseException {
        adapter.open();
        Auto auto = adapter.getEnteredAuto();
        adapter.close();

        int check = 0;
        if(onlyEventBox.isChecked()) check = 1;

        ServiceWork work = new ServiceWork(
                (int) serviceId,
                (int) auto.getId(),
                descrBox.getText().toString(),
                new Date(dateBox.getYear() - 1900, dateBox.getMonth(), dateBox.getDayOfMonth()),
                Integer.parseInt(periodBox.getText().toString()),
                0,
                workTypeColl.get(typeBox.getSelectedItemPosition()).getName(),
                check,
                0
        );

        adapter.open();
        if (serviceId > 0) {
            adapter.updateSW(work);
        } else {
            adapter.insertSW(work);
        }
        adapter.close();
        goHome();
    }
    public void delete(View view){
        adapter.open();
        adapter.deleteSW(serviceId);
        adapter.close();
        goHome();
    }


    private void goHome(){

        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("page", 2);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
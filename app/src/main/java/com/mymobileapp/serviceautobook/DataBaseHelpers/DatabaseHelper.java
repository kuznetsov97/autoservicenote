package com.mymobileapp.serviceautobook.DataBaseHelpers;

import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "serviceAutoBookDb"; // название бд
    private static final int SCHEMA = 1; // версия базы данных

    public static final String COLUMN_ID = "_id";

    public static final String TABLE_AUTOES = "autoes"; // название таблицы в бд
    // названия столбцов
    public static final String COLUMN_MARK = "mark";
    public static final String COLUMN_MODEL = "model";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_NUMBER = "number";
    public static final String COLUMN_PHOTO_PATH = "photo_path";
    public static final String COLUMN_SUM_COST = "sum_cost";
    public static final String COLUMN_ENTER = "entered";

    public static final String TABLE_TYPE_WORKS = "work_types"; // название таблицы в бд
    // названия столбцов
    public static final String COLUMN_NAME = "name";

    public static final String TABLE_UNIT_WORKS = "works_units"; // название таблицы в бд
    // названия столбцов
    public static final String COLUMN_AUTO_ID = "auto_id";
    public static final String COLUMN_WORK_TYPE_ID = "work_type_id";
    public static final String COLUMN_DESCR = "description";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_COST = "cost";
    public static final String COLUMN_WORKER = "worker";
    public static final String COLUMN_KM = "kilometrage";

    public static final String TABLE_SERVICE = "service_works"; // название таблицы в бд
    // названия столбцов
    public static final String COLUMN_DATE_COMPLETE = "date_complete";
    public static final String COLUMN_PERIOD_DATE = "period_date";
    public static final String COLUMN_PERIOD_KM = "period_km";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_IS_ONLY_EVENT = "is_only_event";
    public static final String COLUMN_IS_SYSTEM = "is_system";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_AUTOES + "( "
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_MARK + " TEXT, "
                + COLUMN_MODEL + " TEXT, "
                + COLUMN_YEAR + " INTEGER, "
                + COLUMN_NUMBER + " TEXT, "
                + COLUMN_PHOTO_PATH + " TEXT, "
                + COLUMN_SUM_COST + " TEXT, "
                + COLUMN_ENTER + " INTEGER);");

        db.execSQL("CREATE TABLE " + TABLE_TYPE_WORKS + "( "
                        + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + COLUMN_NAME + " TEXT);");

        db.execSQL("CREATE TABLE " + TABLE_UNIT_WORKS + "( "
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_AUTO_ID + " INTEGER NOT NULL, "
                + COLUMN_WORK_TYPE_ID + " INTEGER NOT NULL, "
                + COLUMN_DESCR + " TEXT, "
                + COLUMN_DATE + " TEXT, "
                + COLUMN_COST + " INTEGER, "
                + COLUMN_WORKER + " TEXT, "
                + COLUMN_KM + " INTEGER, " +
                "FOREIGN KEY (" + COLUMN_WORK_TYPE_ID + ") REFERENCES " + TABLE_TYPE_WORKS + "(" + COLUMN_ID + "), " +
                "FOREIGN KEY (" + COLUMN_AUTO_ID + ") REFERENCES " + TABLE_AUTOES + "(" + COLUMN_ID + "));");

        db.execSQL("CREATE TABLE " + TABLE_SERVICE + "( "
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_AUTO_ID + " INTEGER NOT NULL, "
                + COLUMN_DESCR + " TEXT, "
                + COLUMN_DATE_COMPLETE + " TEXT, "
                + COLUMN_PERIOD_DATE + " INTEGER, "
                + COLUMN_PERIOD_KM + " INTEGER, "
                + COLUMN_TYPE + " TEXT, "
                + COLUMN_IS_ONLY_EVENT + " INTEGER, "
                + COLUMN_IS_SYSTEM + " INTEGER, " +
                "FOREIGN KEY (" + COLUMN_AUTO_ID + ") REFERENCES " + TABLE_AUTOES + "(" + COLUMN_ID + "));");


        // добавление начальных данных таблицы WORK_UNITS
        /*db.execSQL("INSERT INTO "+ TABLE_UNIT_WORKS
                + " ("
                + COLUMN_DESCR + ", "
                + COLUMN_DATE + ", "
                + COLUMN_COST + ", "
                + COLUMN_WORKER + ", "
                + COLUMN_KM
                + ") VALUES ('Замена прокладки ГБЦ', '2018-11-03', 7000, 'Сервис', 98000);");*/

        db.execSQL("INSERT INTO "+ TABLE_AUTOES
                + " ("
                + COLUMN_MARK + ", "
                + COLUMN_MODEL + ", "
                + COLUMN_YEAR + ", "
                + COLUMN_NUMBER + ", "
                + COLUMN_PHOTO_PATH + ", "
                + COLUMN_SUM_COST + ", "
                + COLUMN_ENTER
                + ") VALUES ('ВАЗ', '21124', 2007, 'В329РМ33','', 0, 1);");
        //db.execSQL("INSERT INTO "+ TABLE_AUTOES
        //        + " ("
        //        + COLUMN_MARK + ", "
        //        + COLUMN_MODEL + ", "
        //        + COLUMN_YEAR + ", "
        //        + COLUMN_NUMBER + ", "
        //        + COLUMN_PHOTO_PATH + ", "
        //        + COLUMN_SUM_COST + ", "
        //        + COLUMN_ENTER
        //        + ") VALUES ('ВАЗ', '2101', 1987, 'т256хс37','', 0, 0);");
        //db.execSQL("INSERT INTO "+ TABLE_AUTOES
        //        + " ("
        //        + COLUMN_MARK + ", "
        //        + COLUMN_MODEL + ", "
        //        + COLUMN_YEAR + ", "
        //        + COLUMN_NUMBER + ", "
        //        + COLUMN_PHOTO_PATH + ", "
        //        + COLUMN_SUM_COST + ", "
        //        + COLUMN_ENTER
        //        + ") VALUES ('ВАЗ', '21120', 2004, 'а370кт37','', 0, 0);");

        db.execSQL("INSERT INTO "+ TABLE_TYPE_WORKS
                + " ("
                + COLUMN_NAME
                + ") VALUES ('Подвеска');");
        db.execSQL("INSERT INTO "+ TABLE_TYPE_WORKS
                + " ("
                + COLUMN_NAME
                + ") VALUES ('Двигатель');");
        db.execSQL("INSERT INTO "+ TABLE_TYPE_WORKS
                + " ("
                + COLUMN_NAME
                + ") VALUES ('Трансмиссия');");
        db.execSQL("INSERT INTO "+ TABLE_TYPE_WORKS
                + " ("
                + COLUMN_NAME
                + ") VALUES ('Интерьер');");
        db.execSQL("INSERT INTO "+ TABLE_TYPE_WORKS
                + " ("
                + COLUMN_NAME
                + ") VALUES ('Экстерьер');");
        db.execSQL("INSERT INTO "+ TABLE_TYPE_WORKS
                + " ("
                + COLUMN_NAME
                + ") VALUES ('Прочее');");

        /*db.execSQL("INSERT INTO "+ TABLE_SERVICE
                + " ("
                + COLUMN_AUTO_ID + ", "
                + COLUMN_DESCR + ", "
                + COLUMN_DATE_COMPLETE + ", "
                + COLUMN_PERIOD_DATE + ", "
                + COLUMN_PERIOD_KM + ", "
                + COLUMN_IS_ONLY_EVENT + ", "
                + COLUMN_IS_SYSTEM
                + ") VALUES (1, 'Замена масла' ,'11.03.2018', 6, 8000, 0, 1);");*/
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,  int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_UNIT_WORKS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTOES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TYPE_WORKS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SERVICE);
        onCreate(db);
    }
}
package com.mymobileapp.serviceautobook.DataBaseHelpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.mymobileapp.serviceautobook.DTO.marksAndModelsDTO;
import com.mymobileapp.serviceautobook.Models.*;

public class DatabaseAdapter {

    private SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
    private DatabaseHelper dbHelper;
    private SQLiteDatabase database;

    public DatabaseAdapter(Context context){
        dbHelper = new DatabaseHelper(context.getApplicationContext());
    }

    public void setDatabaseHelperContext(DatabaseHelper db){
        dbHelper = db;
    }

    //public DatabaseAdapter(){}

    public DatabaseAdapter open(){
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close(){
        dbHelper.close();
    }

    private Cursor getAllWorksFromDb(){
        String[] columns = new String[] {DatabaseHelper.COLUMN_ID,
                DatabaseHelper.COLUMN_AUTO_ID,
                DatabaseHelper.COLUMN_WORK_TYPE_ID,
                DatabaseHelper.COLUMN_DESCR,
                DatabaseHelper.COLUMN_DATE,
                DatabaseHelper.COLUMN_COST,
                DatabaseHelper.COLUMN_WORKER,
                DatabaseHelper.COLUMN_KM};
        return  database.query(DatabaseHelper.TABLE_UNIT_WORKS, columns, null, null, null, null, null);
    }
    private Cursor getAllAutoesFromDb(){
        String[] columns = new String[] {DatabaseHelper.COLUMN_ID,
                DatabaseHelper.COLUMN_MARK,
                DatabaseHelper.COLUMN_MODEL,
                DatabaseHelper.COLUMN_YEAR,
                DatabaseHelper.COLUMN_NUMBER,
                DatabaseHelper.COLUMN_PHOTO_PATH,
                DatabaseHelper.COLUMN_SUM_COST,
                DatabaseHelper.COLUMN_ENTER};
        return  database.query(DatabaseHelper.TABLE_AUTOES, columns, null, null, null, null, null);
    }
    private Cursor getAllTypesOfWorkFromDb(){
        String[] columns = new String[] {DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_NAME};
        return  database.query(DatabaseHelper.TABLE_TYPE_WORKS, columns, null, null, null, null, null);
    }
    private Cursor getAllServiceWorksFromDb(){
        String[] columns = new String[] {DatabaseHelper.COLUMN_ID,
                DatabaseHelper.COLUMN_AUTO_ID,
                DatabaseHelper.COLUMN_DESCR,
                DatabaseHelper.COLUMN_DATE_COMPLETE,
                DatabaseHelper.COLUMN_PERIOD_DATE,
                DatabaseHelper.COLUMN_PERIOD_KM};
        return  database.query(DatabaseHelper.TABLE_UNIT_WORKS, columns, null, null, null, null, null);
    }

    ///////////////////////////////////////////////////////////////////////////////

    /**
     * @return Получает из бд сущности "Запись о ремонте" (UnitOfWork) для выбранного автомобиля
     */
    public List<WorkUnit> getAllWorks(Auto auto) {
        ArrayList<WorkUnit> works = new ArrayList<>();
        String query = String.format("SELECT * FROM %s WHERE %s=?",DatabaseHelper.TABLE_UNIT_WORKS, DatabaseHelper.COLUMN_AUTO_ID);
        Cursor cursor = database.rawQuery(query, new String[]{ String.valueOf(auto.getId())});
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID));
                int idAuto = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_AUTO_ID));
                int idTypeWork = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_WORK_TYPE_ID));
                String descr = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DESCR));
                String date = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DATE));
                int cost = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_COST));
                String worker = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_WORKER));
                int km = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_KM));
                try {
                    works.add(new WorkUnit(id, idAuto, idTypeWork, descr, formatter.parse(date), cost, worker, km));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return  works;
    }

    public List<WorkUnit> getAllWorksSort(List<WorkUnit> unsortList) {
        return quickSort(unsortList, 0, unsortList.size()-1);
    }

    /**
     * Быстрая сортировка
     * @param array сортируемый список
     * @param start начальный индекс
     * @param end конечный индекс
     */
    public List<WorkUnit> quickSort (List<WorkUnit> array, int start, int end) {
        if ( start >= end ) {
            return array;
        }

        int marker = start;
        for ( int i = start; i <= end; i++ ) {
            if ( array.get(i).getDate().getTime() <= array.get(end).getDate().getTime() ) {
                WorkUnit temp = array.get(marker); // swap
                WorkUnit temp1 = array.get(i);

                array.remove(marker);
                array.add(marker, temp1);

                array.remove(i);
                array.add(i, temp);

                marker += 1;
            }
        }

        int pivot = marker - 1;
        quickSort (array, start, pivot-1);
        quickSort (array, pivot+1, end);

        return array;
    }


    /**
     * @param id Параметр, по которому ищется запись в бд
     * @return "Запись о ремонте" (UnitOfWork) айди которой приходит на вход
     * @throws ParseException
     */
    public WorkUnit getWorkById(long id) throws ParseException {
        WorkUnit work = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?",DatabaseHelper.TABLE_UNIT_WORKS, DatabaseHelper.COLUMN_ID);
        Cursor cursor = database.rawQuery(query, new String[]{ String.valueOf(id)});
        if(cursor.moveToFirst()){
            int idAuto = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_AUTO_ID));
            int idTypeWork = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_WORK_TYPE_ID));
            String descr = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DESCR));
            String date = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DATE));
            int cost = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_COST));
            String worker = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_WORKER));
            int km = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_KM));
            work = new WorkUnit((int)id, idAuto, idTypeWork, descr, formatter.parse(date), cost, worker, km);
        }
        cursor.close();
        return work;
    }

    /**
     * @param work Объект "Запись о ремонте" (UnitOfWork), который нужно добавить в бд
     * @return Добавляет в базу данных сущность "Запись о ремонте" (UnitOfWork)
     */
    public long insertUOW(WorkUnit work){

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_AUTO_ID, work.getAutoId());
        cv.put(DatabaseHelper.COLUMN_WORK_TYPE_ID, work.getWorkTypeId());
        cv.put(DatabaseHelper.COLUMN_DATE, formatter.format(work.getDate()));
        cv.put(DatabaseHelper.COLUMN_COST, work.getCost());
        cv.put(DatabaseHelper.COLUMN_DESCR, work.getDescription());
        cv.put(DatabaseHelper.COLUMN_KM, work.getKm());
        cv.put(DatabaseHelper.COLUMN_WORKER, work.getWorker());

        return  database.insert(DatabaseHelper.TABLE_UNIT_WORKS, null, cv);
    }

    /**
     * @param userId айди сущности "Запись о ремонте" (UnitOfWork), которую нужно удалить из бд
     * @return Удаляет сущность "Запись о ремонте" (UnitOfWork) из бд
     */
    public long deleteUOW(long userId){

        String whereClause = "_id = ?";
        String[] whereArgs = new String[]{String.valueOf(userId)};
        return database.delete(DatabaseHelper.TABLE_UNIT_WORKS, whereClause, whereArgs);
    }

    /**
     * @param work Объект "Запись о ремонте" (UnitOfWork), который нужно обновить в бд
     * @return Обновляет в базе данных сущность "Запись о ремонте" (UnitOfWork)
     */
    public long updateUOW(WorkUnit work){

        String whereClause = DatabaseHelper.COLUMN_ID + "=" + String.valueOf(work.getId());
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_AUTO_ID, work.getAutoId());
        cv.put(DatabaseHelper.COLUMN_WORK_TYPE_ID, work.getWorkTypeId());
        cv.put(DatabaseHelper.COLUMN_DATE, formatter.format(work.getDate()));
        cv.put(DatabaseHelper.COLUMN_COST, work.getCost());
        cv.put(DatabaseHelper.COLUMN_DESCR, work.getDescription());
        cv.put(DatabaseHelper.COLUMN_KM, work.getKm());
        cv.put(DatabaseHelper.COLUMN_WORKER, work.getWorker());

        return database.update(DatabaseHelper.TABLE_UNIT_WORKS, cv, whereClause, null);
    }

    ///////////////////////////////////////////////////////////////////////////////

    /**
     * @return Получает из бд все сущности "Автомобиль" (auto)
     */
    public List<Auto> getAllAutoes() {
        ArrayList<Auto> autoes = new ArrayList<>();
        Cursor cursor = getAllAutoesFromDb();
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID));
                String mark = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_MARK));
                String model = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_MODEL));
                int year = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_YEAR));
                String number = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NUMBER));
                String photoPath = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PHOTO_PATH));
                int sumCost = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_SUM_COST));
                int entered = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ENTER));
                autoes.add(new Auto(id, mark, model, year, number, photoPath, sumCost, entered));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return  autoes;
    }

    /**
     * @return Получает из бд выбранную сущность "Автомобиль" (auto)
     */
    public Auto getEnteredAuto() {
        Auto auto= null;
        String query = String.format("SELECT * FROM %s WHERE %s=?",DatabaseHelper.TABLE_AUTOES, DatabaseHelper.COLUMN_ENTER);
        Cursor cursor = database.rawQuery(query, new String[]{ String.valueOf(1)});
        if(cursor.moveToFirst()){
                int id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID));
                String mark = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_MARK));
                String model = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_MODEL));
                int year = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_YEAR));
                String number = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NUMBER));
                String photoPath = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PHOTO_PATH));
                int sumCost = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_SUM_COST));
                int entered = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ENTER));

                auto = new Auto(id, mark, model, year, number, photoPath, sumCost, entered);
        }
        cursor.close();
        return  auto;
    }

    /**
     * Изменяет автобобиль который выбран в данный момент
     * @param id айди авто, который только что выбран
     */
    public void setEnteredAuto(long id) {
        Auto old = getEnteredAuto();
        Auto now = getAutoById(id);

        if(old != null) {
            old.setEntered(0);
            updateAuto(old);
        }
        if(now!= null) {
            now.setEntered(1);
            updateAuto(now);
        }
    }

    /**
     * @return Количество записей в таблице Автомобилей
     */
    public int getCountAutoes(){
        return (int)DatabaseUtils.queryNumEntries(database, DatabaseHelper.TABLE_AUTOES);
    }

    /**
     * @param id Параметр, по которому ищется запись в бд
     * @return "Автомобиль" (auto) айди которой приходит на вход
     */
    public Auto getAutoById(long id){
        Auto auto = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?",DatabaseHelper.TABLE_AUTOES, DatabaseHelper.COLUMN_ID);
        Cursor cursor = database.rawQuery(query, new String[]{ String.valueOf(id)});
        if(cursor.moveToFirst()){
            String mark = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_MARK));
            String model = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_MODEL));
            int year = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_YEAR));
            String number = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NUMBER));
            String photoPath = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_PHOTO_PATH));
            int sumCost = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_SUM_COST));
            int entered = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ENTER));

            auto = new Auto((int)id, mark, model, year, number, photoPath, sumCost, entered);
        }
        cursor.close();
        return auto;
    }

    /**
     * @param auto Объект "Автомобиль" (auto), который нужно добавить в бд
     * @return Добавляет в базу данных сущность "Автомобиль" (auto)
     */
    public long insertAuto(Auto auto){

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_MARK, auto.getMark());
        cv.put(DatabaseHelper.COLUMN_MODEL, auto.getModel());
        cv.put(DatabaseHelper.COLUMN_YEAR, auto.getYear());
        cv.put(DatabaseHelper.COLUMN_NUMBER, auto.getNumber());
        cv.put(DatabaseHelper.COLUMN_PHOTO_PATH, auto.getPhotoPath());
        cv.put(DatabaseHelper.COLUMN_SUM_COST, auto.getSumCost());
        cv.put(DatabaseHelper.COLUMN_ENTER, auto.getEntered());

        return  database.insert(DatabaseHelper.TABLE_AUTOES, null, cv);
    }

    /**
     * @param autoId айди сущности "Автомобиль" (auto), которую нужно удалить из бд
     * @return Удаляет сущность "Автомобиль" (auto) из бд
     */
    public long deleteAuto(long autoId){

        String whereClause = "_id = ?";
        String[] whereArgs = new String[]{String.valueOf(autoId)};
        return database.delete(DatabaseHelper.TABLE_AUTOES, whereClause, whereArgs);
    }

    /**
     * @param auto Объект "Автомобиль" (auto), который нужно обновить в бд
     * @return Обновляет в базе данных сущность "Автомобиль" (auto)
     */
    public long updateAuto(Auto auto){

        String whereClause = DatabaseHelper.COLUMN_ID + "=" + String.valueOf(auto.getId());
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_MARK, auto.getMark());
        cv.put(DatabaseHelper.COLUMN_MODEL, auto.getModel());
        cv.put(DatabaseHelper.COLUMN_YEAR, auto.getYear());
        cv.put(DatabaseHelper.COLUMN_NUMBER, auto.getNumber());
        cv.put(DatabaseHelper.COLUMN_PHOTO_PATH, auto.getPhotoPath());
        cv.put(DatabaseHelper.COLUMN_SUM_COST, auto.getSumCost());
        cv.put(DatabaseHelper.COLUMN_ENTER, auto.getEntered());

        return database.update(DatabaseHelper.TABLE_AUTOES, cv, whereClause, null);
    }

    ///////////////////////////////////////////////////////////////////////////////

    /**
     * @return Получить все объекты "Тип работы" из бд
     */
    public List<WorkType> getAllTypesOfWork() {
        ArrayList<WorkType> types = new ArrayList<>();
        Cursor cursor = getAllTypesOfWorkFromDb();
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID));
                String name = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NAME));
                types.add(new WorkType(id, name));
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return  types;
    }

    /**
     * @param id Параметр, по которому ищется запись в бд
     * @return "Тип работы" (WorkType) айди которой приходит на вход
     * @throws ParseException
     */
    public WorkType getWorkTypeById(long id){
        WorkType type = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?",DatabaseHelper.TABLE_TYPE_WORKS, DatabaseHelper.COLUMN_ID);
        Cursor cursor = database.rawQuery(query, new String[]{ String.valueOf(id)});
        if(cursor.moveToFirst()){
            String name = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NAME));
            type = new WorkType((int)id, name);
        }
        cursor.close();
        return type;
    }

    /**
     * @param name Параметр, по которому ищется запись в бд
     * @return "Тип работы" (WorkType) название которой приходит на вход
     * @throws ParseException
     */
    public WorkType getWorkTypeByName(String name){
        WorkType type = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?",DatabaseHelper.TABLE_TYPE_WORKS, DatabaseHelper.COLUMN_NAME);
        Cursor cursor = database.rawQuery(query, new String[]{ String.valueOf(name)});
        if(cursor.moveToFirst()){
            int id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID));
            type = new WorkType(id, name);
        }
        cursor.close();
        return type;
    }

    ///////////////////////////////////////////////////////////////////////////////

    /**
     * @return Получает из бд сущности "Сервисная запись" (ServiceWork) для выбранного автомобиля
     */
    public List<ServiceWork> getAllServiceWorks(Auto auto) {
        ArrayList<ServiceWork> services = new ArrayList<>();
        String query = String.format("SELECT * FROM %s WHERE %s=?",DatabaseHelper.TABLE_SERVICE, DatabaseHelper.COLUMN_AUTO_ID);
        Cursor cursor = database.rawQuery(query, new String[]{ String.valueOf(auto.getId())});
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID));
                int idAuto = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_AUTO_ID));
                String descr = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DESCR));
                String dateCompl = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DATE_COMPLETE));
                int periodDate = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_PERIOD_DATE));
                int periodKm = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_PERIOD_KM));
                String type = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_TYPE));
                int isOnlyEvent = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_IS_ONLY_EVENT));
                int isSystem = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_IS_SYSTEM));

                try {
                    services.add(new ServiceWork(id, idAuto, descr, formatter.parse(dateCompl), periodDate, periodKm, type, isOnlyEvent, isSystem));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        return  services;
    }

    /**
     * @param id Параметр, по которому ищется запись в бд
     * @return "Сервисная запись" (ServiceWork) айди которой приходит на вход
     * @throws ParseException
     */
    public ServiceWork getServiceWorkById(long id) throws ParseException {
        ServiceWork work = null;
        String query = String.format("SELECT * FROM %s WHERE %s=?",DatabaseHelper.TABLE_SERVICE, DatabaseHelper.COLUMN_ID);
        Cursor cursor = database.rawQuery(query, new String[]{ String.valueOf(id)});
        if(cursor.moveToFirst()){
            int idAuto = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_AUTO_ID));
            String descr = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DESCR));
            String dateCompl = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_DATE_COMPLETE));
            int periodDate = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_PERIOD_DATE));
            int periodKm = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_PERIOD_KM));
            String type = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_TYPE));
            int isOnlyEvent = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_IS_ONLY_EVENT));
            int isSystem = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_IS_SYSTEM));
            work = new ServiceWork((int)id, idAuto, descr, formatter.parse(dateCompl), periodDate, periodKm, type, isOnlyEvent, isSystem);
        }
        cursor.close();
        return work;
    }

    /**
     * @param work Объект "Сервисная запись" (ServiceWork), который нужно добавить в бд
     * @return Добавляет в базу данных сущность "Сервисная запись" (ServiceWork)
     */
    public long insertSW(ServiceWork work){

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_AUTO_ID, work.getAutoId());
        cv.put(DatabaseHelper.COLUMN_DESCR, work.getDescription());
        cv.put(DatabaseHelper.COLUMN_DATE_COMPLETE, formatter.format(work.getDateComplete()));
        cv.put(DatabaseHelper.COLUMN_PERIOD_DATE, work.getPeriodDate());
        cv.put(DatabaseHelper.COLUMN_PERIOD_KM, work.getPeriodKm());
        cv.put(DatabaseHelper.COLUMN_TYPE, work.getType());
        cv.put(DatabaseHelper.COLUMN_IS_ONLY_EVENT, work.getIsOnlyEvent());
        cv.put(DatabaseHelper.COLUMN_IS_SYSTEM, work.getIsSystem());

        return  database.insert(DatabaseHelper.TABLE_SERVICE, null, cv);
    }

    /**
     * @param userId айди сущности "Сервисная запись" (ServiceWork), которую нужно удалить из бд
     * @return Удаляет сущность "Сервисная запись" (ServiceWork) из бд
     */
    public long deleteSW(long userId){

        String whereClause = "_id = ?";
        String[] whereArgs = new String[]{String.valueOf(userId)};
        return database.delete(DatabaseHelper.TABLE_SERVICE, whereClause, whereArgs);
    }

    /**
     * @param work Объект "Запись о ремонте" (UnitOfWork), который нужно обновить в бд
     * @return Обновляет в базе данных сущность "Запись о ремонте" (UnitOfWork)
     */
    public long updateSW(ServiceWork work){

        String whereClause = DatabaseHelper.COLUMN_ID + "=" + String.valueOf(work.getId());
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.COLUMN_AUTO_ID, work.getAutoId());
        cv.put(DatabaseHelper.COLUMN_DESCR, work.getDescription());
        cv.put(DatabaseHelper.COLUMN_DATE_COMPLETE, formatter.format(work.getDateComplete()));
        cv.put(DatabaseHelper.COLUMN_PERIOD_DATE, work.getPeriodDate());
        cv.put(DatabaseHelper.COLUMN_PERIOD_KM, work.getPeriodKm());
        cv.put(DatabaseHelper.COLUMN_TYPE, work.getType());
        cv.put(DatabaseHelper.COLUMN_IS_ONLY_EVENT, work.getIsOnlyEvent());
        cv.put(DatabaseHelper.COLUMN_IS_SYSTEM, work.getIsSystem());

        return database.update(DatabaseHelper.TABLE_SERVICE, cv, whereClause, null);
    }


    public List<marksAndModelsDTO> markModel;
}
